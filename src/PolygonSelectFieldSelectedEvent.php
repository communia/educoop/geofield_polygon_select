<?php

namespace Drupal\geofield_polygon_select;

use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 *
 */
class PolygonSelectFieldSelectedEvent extends Event {

  const SUBMIT = 'event.submit';

  /**
   * The Entity that is storing the polygon.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The field name that holds the polygons.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The values that are used to populate the field.
   *
   * @var array
   */
  protected $values;

  /**
   * PolygonSelectFieldSelectedEvent constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The Entity that is storing the polygon.
   * @param string $field_name
   *   The field name that holds the polygons.
   * @param array $values
   *   The values that are used to populate the field.
   */
  public function __construct(EntityInterface $entity, $field_name, $values) {
    $this->entity = $entity;
    $this->fieldName = $field_name;
    $this->values = $values;
  }

  /**
   * Return entity that is storing the polygon.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Return field name that holds the polygons.
   *
   * @return string
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * Return values from form that are used to populate the field.
   *
   * @return array
   */
  public function getValues() {
    return $this->values;
  }

  /**
   *
   */
  public function eventDescription() {
    return "This event is dispatched whenever a polygon from Polygon Select Field is added as field to an entity";
  }

}
