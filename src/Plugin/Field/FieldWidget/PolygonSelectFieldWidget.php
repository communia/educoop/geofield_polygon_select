<?php

namespace Drupal\geofield_polygon_select\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\geofield\Plugin\Field\FieldWidget\GeofieldDefaultWidget;
use Drupal\geofield_polygon_select\Entity\PolygonFeatureCollectionInterface;
use Drupal\geofield_polygon_select\Entity\PolygonFeatureCollection;
use Drupal\geofield_polygon_select\FeatureCollectionStore;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\geofield_polygon_select\PolygonSelectFieldSelectedEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\geofield\WktGeneratorInterface;
use GeoJson\GeoJson;

/**
 * Plugin implementation of the 'geofield_polygon_select' widget.
 *
 * @FieldWidget(
 *   id = "geofield_polygon_select",
 *   module = "geofield_polygon_select",
 *   label = @Translation("Polygon select field widget"),
 *   field_types = {
 *     "geofield",
 *     "geofield_polygon_item"
 *   }
 * )
 */
class PolygonSelectFieldWidget extends GeofieldDefaultWidget {
  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The service to fetch features from the feature collections entities.
   *
   * @var \Drupal\geofield_polygon_select\FeatureCollectionStore
   */
  protected $featureCollectionStore;

  /**
   * Constructs a PolygonSelectFieldWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geophp_wrapper
   *   The geoPhpWrapper.
   * @param \Drupal\geofield\WktGeneratorInterface $wkt_generator
   *   The WKT format Generator service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The Event dispatcher.
   * @param \Drupal\geofield_polygon_select\FeatureCollectionStore
   *   $feature_collection_store
   *   The service to fetch features from the feature collections entities.
   */
  public function __construct($plugin_id,
  $plugin_definition,
  FieldDefinitionInterface $field_definition,
  array $settings,
  array $third_party_settings,
  GeoPHPInterface $geophp_wrapper,
    WktGeneratorInterface $wkt_generator,
    EventDispatcherInterface $event_dispatcher,
    FeatureCollectionStore $feature_collection_store
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings, $geophp_wrapper, $wkt_generator);
    $this->eventDispatcher = $event_dispatcher;
    $this->featureCollectionStore = $feature_collection_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('geofield.geophp'),
      $container->get('geofield.wkt_generator'),
      $container->get('event_dispatcher'),
      $container->get('geofield_polygon_select.feature_collection_store')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'collections' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $cols = \Drupal::entityTypeManager()->getHandler('polygon_feature_collection', 'widget_settings_list_builder')
      ->setSettings($this->getSetting('collections'))
      ->render();
    // Take result and specifically table to build collections table (if want
    // something else from $cols take it apart)
    $elements['collections'] = $cols['table'];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if (!empty($this->getSetting('collections'))) {
      $summary[] = t('collections present');
    }
    if (!empty($this->getSetting('collections'))) {
      $summary[] = t(': @collections', ['@collections' => implode(",", array_keys($this->getSetting('collections')))]);
    }

    return $summary;
  }

  /**
   * Get the options from list of polygon feature collections entities created.
   *
   * @var \Drupal\geofield_polygon_select\Entity\PolygonFeatureCollectionInterface $collection_entity
   *   The collection entity to extract options.
   */
  private function optionsFromJson(PolygonFeatureCollectionInterface $collection_entity) {
    // TODO sort collections from $this->getSetting('')
    // TODO foreach collections in $sorted_collections
    // create select.
    $options = [];
    $json = json_decode($collection_entity->get('geoJson'));
    /* @var \GeoJson $feature_collection */
    $feature_collection = GeoJson::jsonUnserialize($json);
    foreach ($feature_collection->getFeatures() as $id => $feature) {
      if (isset($feature->getProperties()[$collection_entity->get('keyholder')])) {
        if ($this->fieldDefinition->getType() == "geofield") {
          // If we want to add as key directly the geometry
          // $options[json_encode($feature->getGeometry())] = $feature->getProperties()[$collection_entity->get('keyholder')];
          $options[$feature->getProperties()[$collection_entity->get('keyholder')]] = $feature->getProperties()[$collection_entity->get('keyholder')];
        }
        if ($this->fieldDefinition->getType() == "geofield_polygon_item") {
          // If we want to add as key directly the feature:
          // $options[json_encode($feature->jsonSerialize())] = $feature->getProperties()[$collection_entity->get('keyholder')];
          $options[$feature->getProperties()[$collection_entity->get('keyholder')]] = $feature->getProperties()[$collection_entity->get('keyholder')];
        }
      }
    }
    if (empty($options)) {
      $options[''] = t('No defined polygons');
    }
    return $options;
  }

  /**
   * Collection types options maker.
   *
   * @param array $collections
   *   Array of polygon feature collection entity.
   *
   * @return array
   *   Options with enabled collections types to be used in select form element.
   */
  private function polygonFeatureCollectionsOptions(array $collections) {
    // $options = ["" => $this->t("None")];
    $options = [];
    foreach ($collections as $collection_id => $collection) {
      $options[$collection_id] = $collection->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if ($this->fieldDefinition->getType() == "geofield") {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }

    $full_collections = PolygonFeatureCollection::loadMultiple($this->enabledCollections());
    /** @var [entity_id => $entity->label() ] of a PolygonFeatureCollection **/
    $geographical_scope_options = $this->polygonFeatureCollectionsOptions($full_collections);

    $geographical_scope_default_value = isset($items[$delta]->feature_collection) ? $items[$delta]->feature_collection : NULL;
    $element["geographical_scope"] = [
      '#type' => 'select',
      '#title' => $this->t('Geographical scope'),
      '#options' => $geographical_scope_options,
      '#default_value' => $geographical_scope_default_value,
      '#empty_value' => '',
      '#states' => [
        // Prepare array to be filled by each collection
    //        'enabled' =>.
      ],
    ];

    // Prepare the list of options.
    foreach ($this->enabledCollections() as $weight => $collection_id) {
      $element["geographical_scope"]['#states']['invisible'][] = [
        'select[name="' . $this->fieldDefinition->getName() . '[' . $delta . '][' . $collection_id . ']"]' => ['!value' => ""],
      ];
      $label = "";
      $collection_options = $this->optionsFromJson($full_collections[$collection_id]);

      if (empty($collection_options)) {
        $collection_options[''] = t('No defined polygons');
      }

      // default_value logic.
      if (isset($items[$delta]->feature_collection)) {
        if ($items[$delta]->feature_collection == $collection_id) {
          $default_value = isset($items[$delta]->feature) ? $items[$delta]->feature : NULL;
        }
      }

      $element[$collection_id] = [
        '#type' => 'select',
          // Reuse the options array as we can take the label there.
        '#title' => $geographical_scope_options[$collection_id],
        '#default_value' => isset($default_value) ? $default_value : NULL,
        '#options' => $collection_options,
        '#empty_value' => '',
        '#attached' => [
          'library' => [
              // 'color/drupal.color',
              // 'color/admin',
            ],
        ],
        '#states' => [
          'visible' => [
            'select[name="' . $this->fieldDefinition->getName() . '[' . $delta . '][geographical_scope]"]' => ['value' => $collection_id],
          ],
        ],
      ];

    }

    return $element;
  }

  /**
   * Output just the enabled collections keyed by weight.
   *
   * @return array
   *   The enabled collections keyed by weight.
   */
  public function enabledCollections() {
    $enabled = [];
    foreach ($this->settings["collections"] as $collection_id => $collection_props) {
      if ($collection_props["enabled"]) {
        $enabled[$collection_props["weight"]] = $collection_id;
      }
    }
    return $enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $shapes) {
      // Check first if preexistent value in field is empty.
      foreach ($shapes as $shape_key => $shape) {
        if (isset($shape["value"]) && empty($shape["value"])) {
          unset($values[$delta]);
          continue;
        }
        // Now check if form input values are fine for geophp.
        foreach ($this->enabledCollections() as $collection_name) {
          if ($shape_key == $collection_name) {
            if (!empty($shape)) {
              $feature = $this->featureCollectionStore->getFeatureFromKey($collection_name, $shape);
              /* @var \Geometry $geom */
              if ($geom = $this->geoPhpWrapper->load($feature)) {
                if ($this->fieldDefinition->getType() == "geofield") {
                  $values[$delta]['value'] = $geom->out('wkt');
                }
                if ($this->fieldDefinition->getType() == "geofield_polygon_item") {
                  $values[$delta]['value'] = $geom->out('wkt');
                  $values[$delta]['geojson'] = $feature;
                  $values[$delta]['feature_collection'] = $collection_name;
                  // $this->featureCollectionStore->getKeyFromFeature($collection_name,$shape);
                  $values[$delta]['feature'] = $shape;
                }
              }
            }
          }
        }
      }
    }
    // Dispatch event.
    $entity = $form_state->getBuildInfo()["callback_object"]->getEntity();
    $field = $this->fieldDefinition->getName();
    $event = new PolygonSelectFieldSelectedEvent($entity, $field, $values);
    $this->eventDispatcher->dispatch(PolygonSelectFieldSelectedEvent::SUBMIT, $event);
    return $values;
  }

}
