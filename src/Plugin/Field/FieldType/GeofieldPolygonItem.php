<?php

namespace Drupal\geofield_polygon_select\Plugin\Field\FieldType;

// Use Drupal\Core\Field\FieldDefinitionInterface;
// use Drupal\Core\Field\FieldItemBase;.
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\geofield\Plugin\Field\FieldType\GeofieldItem;

/**
 * Plugin implementation of the 'geofield_polygon_item' field type.
 *
 * @FieldType(
 *   id = "geofield_polygon_item",
 *   label = @Translation("Geofield polygon item"),
 *   description = @Translation("Geofield Field with geojson metadata"),
 *   default_widget = "geofield_polygon_select",
 *   default_formatter = "polygon_select_field_formatter"
 * )
 */
class GeofieldPolygonItem extends GeofieldItem {

  /**
   * {@inheritdoc}
   * public static function defaultStorageSettings() {
   * return [
   * 'max_length' => 255,
   * 'is_ascii' => FALSE,
   * 'case_sensitive' => FALSE,
   * ] + parent::defaultStorageSettings();
   * }.
   */

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'geofield_target' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslatableMarkup.
    $properties['geojson'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('GeoJSON Value'));
    $properties['feature_collection'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Feature collection'));
    $properties['feature'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Feature'));
    return $properties + parent::propertyDefinitions($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['geojson'] = [
      'type' => 'text',
      'size' => 'big',
      'not null' => FALSE,
    ];

    $schema['columns']['feature_collection'] = [
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ];
    $schema['columns']['feature'] = [
      'type' => 'varchar',
      'length' => 255,
      'not null' => FALSE,
    ];
    $schema['indexes']['geojson'] = ['geojson'];
    $schema['indexes']['feature_collection'] = ['feature_collection'];
    $schema['indexes']['feature'] = ['feature'];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    if ($this->getFieldDefinition()->getType() == "geofield_polygon_item") {
      /** @var \Drupal\Core\Field\TypedData\FieldItemDataDefinition  **/
      $definition = $this->getFieldDefinition();
      // Add geofield_target only if we are not working with geofield fieldtype.
      $entity_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($this->getEntity()->getEntityTypeId(), $this->getEntity()->bundle());

      $geofields = [
        "" => t("- None -"),
      ];
      foreach ($entity_fields as $field_name => $field) {
        if ($field->getType() == 'geofield') {
          $geofields[$field_name] = $field_name . " - " . $field->getLabel();
        }
      }
      $elements['geofield_target'] = [
        '#type' => 'select',
        '#options' => $geofields,
        '#title' => t('Geofield target'),
        '#default_value' => $this->getSetting('geofield_target'),
        '#description' => t('The geofield field machine name that will be synced as slave with this field.'),
      ];
    }
    return $elements + parent::fieldSettingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   * public function getConstraints() {
   * $constraints = parent::getConstraints();.
   *
   * If ($max_length = $this->getSetting('max_length')) {
   * $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
   * $constraints[] = $constraint_manager->create('ComplexData', [
   * 'value' => [
   * 'Length' => [
   * 'max' => $max_length,
   * 'maxMessage' => t('%name: may not be longer than @max characters.', [
   * '%name' => $this->getFieldDefinition()->getLabel(),
   * '@max' => $max_length
   * ]),
   * ],
   * ],
   * ]);
   * }
   *
   * return $constraints;
   * }
   */

  /**
   * {@inheritdoc}
   * public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
   * $random = new Random();
   * $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
   * return $values;
   * }.
   */

  /**
   * {@inheritdoc}
   * public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
   * $elements = [];.
   *
   * $elements['max_length'] = [
   * '#type' => 'number',
   * '#title' => t('Maximum length'),
   * '#default_value' => $this->getSetting('max_length'),
   * '#required' => TRUE,
   * '#description' => t('The maximum length of the field in characters.'),
   * '#min' => 1,
   * '#disabled' => $has_data,
   * ];
   *
   * return $elements;
   * }
   */

  /**
   * {@inheritdoc}
   * public function isEmpty() {
   * $value = $this->get('value')->getValue();
   * return $value === NULL || $value === '';
   * }.
   */

}
