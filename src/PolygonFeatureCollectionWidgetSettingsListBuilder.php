<?php

namespace Drupal\geofield_polygon_select;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Polygon feature collection entities to be rendered
 * in a field widget settings to choose which one to use and in what order.
 */
class PolygonFeatureCollectionWidgetSettingsListBuilder extends ConfigEntityListBuilder {

  /**
   * Array with PolygonFeatureCollection coming from widget, following schema
   *   ["collection_name" => [
   *     "enabled" => bool, "weight" => int
   *     ],
   *   ].
   *
   * @var array
   */
  protected $settings = [];

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Polygon feature collection');
    $header['enabled'] = t('Enabled');
    $header['weight'] = t('Weight');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function setSelection(array $selection) {
    $this->selection = $selection;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#tabledrag'] = [
      [
        'action' => 'order',
        'relationship' => 'sibling',
        'group' => 'weight',
      ],
    ];
    // Move #rows to own array position to suit draggable row behaviors.
    foreach ($build['table']['#rows'] as $collection => $row) {
      $build['table'][$collection] = $row;
      /*if (isset($settings['collections'][$collection])){
      $build['table'][$collection]['weight']['default_value'] =
      }*/
    }
    unset($build['table']['#rows']);
    // kint($build);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    // SEGUIR COPIANT IMPLEMENTACIO DE DRAGGABLE pero sense formulari ja que lagafarà de widget settings i no podem posar-ne un d'aniuat!!!!
    $row['label'] = ['#markup' => $entity->label()];
    $row['enabled'] = [
      '#type' => 'checkbox',
      '#default_value' => (in_array($entity->id(), array_keys($this->settings))) ? $this->settings[$entity->id()]['enabled'] : FALSE,
    ];
    // Override default values to markup elements.
    $row['#attributes']['class'][] = 'draggable';
    // Add weight column.
    $row['#weight'] = (in_array($entity->id(), array_keys($this->settings))) ? $this->settings[$entity->id()]['weight'] : 0;
    $row['weight'] = [
      '#type' => 'weight',
      '#title' => t('Weight for @title', [
        '@title' => $entity
          ->label(),
      ]),
      '#title_display' => 'invisible',
      '#default_value' => (in_array($entity->id(), array_keys($this->settings))) ? $this->settings[$entity->id()]['weight'] : 0,
      '#attributes' => [
        'class' => [
          'weight',
        ],
      ],
    ];

    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function setSettings(array $settings) {
    $this->settings = $settings;
    return $this;
  }

}
