<?php

namespace Drupal\geofield_polygon_select\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Polygon feature collection entity.
 *
 * @ConfigEntityType(
 *   id = "polygon_feature_collection",
 *   label = @Translation("Polygon feature collection"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\geofield_polygon_select\PolygonFeatureCollectionListBuilder",
 *     "widget_settings_list_builder" = "Drupal\geofield_polygon_select\PolygonFeatureCollectionWidgetSettingsListBuilder",
 *     "form" = {
 *       "add" = "Drupal\geofield_polygon_select\Form\PolygonFeatureCollectionForm",
 *       "edit" = "Drupal\geofield_polygon_select\Form\PolygonFeatureCollectionForm",
 *       "delete" = "Drupal\geofield_polygon_select\Form\PolygonFeatureCollectionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\geofield_polygon_select\PolygonFeatureCollectionHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "polygon_feature_collection",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/geofield_polygons_providers/polygon_feature_collection/{polygon_feature_collection}",
 *     "add-form" = "/admin/config/system/geofield_polygons_providers/polygon_feature_collection/add",
 *     "edit-form" = "/admin/config/system/geofield_polygons_providers/polygon_feature_collection/{polygon_feature_collection}/edit",
 *     "delete-form" = "/admin/config/system/geofield_polygons_providers/polygon_feature_collection/{polygon_feature_collection}/delete",
 *     "collection" = "/admin/config/system/geofield_polygons_providers/polygon_feature_collection"
 *   }
 * )
 */
class PolygonFeatureCollection extends ConfigEntityBase implements PolygonFeatureCollectionInterface {

  /**
   * The Polygon feature collection ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Polygon feature collection label.
   *
   * @var string
   */
  protected $label;

  /**
   * The property holding the text key to identitify the polygon.
   *
   * @var string
   */
  protected $keyholder;

  /**
   * The geoJson file that stores the feature collection.
   *
   * @var string
   */
  protected $geoJson;

  /**
   * {@inheritdoc}
   */
  public function setGeoJson($geo_json) {
    $this->geoJson = $geo_json;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGeoJson() {
    return $this->geoJson;
  }

  /**
   * {@inheritdoc}
   */
  public function setKeyholder($keyholder) {
    $this->keyholder = $keyholder;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyholder() {
    return $this->keyholder;
  }

}
