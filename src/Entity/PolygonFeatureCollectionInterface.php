<?php

namespace Drupal\geofield_polygon_select\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Polygon feature collection entities.
 */
interface PolygonFeatureCollectionInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Sets the geojson with the featurecollection of polygons.
   *
   * @param string $geoJson
   *   The geojson that contains the featurecollection of polygons features.
   *
   * @return $this
   */
  public function setGeoJson($geoJson);

  /**
   * Returns the geojson that contains the featurecollection of polygons.
   *
   * @return string
   *   The geojson that contains the featurecollection of polygons features.
   */
  public function getGeoJson();

  /**
   * Sets the keyholder for the polygon featurecollection in geojson.
   *
   * @param string $keyholder
   *   The key that is referencing the name identifying the feature.
   *
   * @return $this
   */
  public function setKeyholder($keyholder);

  /**
   * Returns the keyholder for the featurecollection in geojson.
   *
   * @return string
   *   The keyholder.
   */
  public function getKeyholder();

}
