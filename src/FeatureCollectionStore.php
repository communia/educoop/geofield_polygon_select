<?php

namespace Drupal\geofield_polygon_select;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use GeoJson\GeoJson;

/**
 * Class FeatureCollectionStore.
 */
class FeatureCollectionStore implements FeatureCollectionStoreInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new FeatureCollectionStore object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFeatureFromKey($collection_id, $key) {
    if ($collection = $this->entityTypeManager->getStorage('polygon_feature_collection')->load($collection_id)) {
      $keyholder = $collection->getKeyholder();
      $geoJson = json_decode($collection->getGeoJson());
      $feature_collection = GeoJson::jsonUnserialize($geoJson);
      foreach ($feature_collection->getFeatures() as $id => $feature) {
        if (isset($feature->getProperties()[$collection->getKeyholder()])) {
          if ($feature->getProperties()[$collection->getKeyholder()] == $key) {
            return json_encode($feature->jsonSerialize());
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getKeyFromFeature($collection_id, $feature) {
    if ($collection = $this->entityTypeManager->getStorage('polygon_feature_collection')->load($collection_id)) {
      $keyholder = $collection->getKeyholder();
      $feature_decoded = json_decode($feature);
      $feature_parsed = GeoJson::jsonUnserialize($feature_decoded);
      if (isset($feature_parsed->getProperties()[$collection_entity->getKeyholder()])) {
        return $feature_parsed->getProperties()[$collection_entity->getKeyholder()];
      }
    }
  }

}
