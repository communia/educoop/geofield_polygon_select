<?php

namespace Drupal\geofield_polygon_select\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PolygonFeatureCollectionForm.
 */
class PolygonFeatureCollectionForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $polygon_feature_collection = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $polygon_feature_collection->label(),
      '#description' => $this->t("Label for the Polygon feature collection."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $polygon_feature_collection->id(),
      '#machine_name' => [
        'exists' => '\Drupal\geofield_polygon_select\Entity\PolygonFeatureCollection::load',
      ],
      '#disabled' => !$polygon_feature_collection->isNew(),
    ];

    $form['geoJson'] = [
      '#type' => 'textarea',
      '#title' => t('GeoJson'),
      '#default_value' => $polygon_feature_collection->getGeoJson(),
    ];

    $form['keyholder'] = [
      '#type' => 'textfield',
      '#title' => t('Keyholder'),
      '#default_value' => $polygon_feature_collection->getKeyholder(),
      '#description' => t('Key that contains the name describbing each polygon in geojson, such as city_name.'),
    ];
    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $polygon_feature_collection = $this->entity;
    $status = $polygon_feature_collection->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Polygon feature collection.', [
          '%label' => $polygon_feature_collection->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Polygon feature collection.', [
          '%label' => $polygon_feature_collection->label(),
        ]));
    }
    $form_state->setRedirectUrl($polygon_feature_collection->toUrl('collection'));
  }

}
