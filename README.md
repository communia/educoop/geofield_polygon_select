# Geofield polygon select

Auxiliar config entity to store json polygon collections. It will populate a select in field form widget. To choose which ones are available for each field choose it in field form display settings (select the cog)· There you can order the weight of the collections and select which ones are active.
This will have distinct behaviors depending on which field the data is stored.
## Behavior when using the widget in a geofield field
If pure geofield is used it will only populate a geofield as wkt.
At this step this will dispatch an event with Entity, Shape picked, Key of this shape, and Config entity id referenced. As it will only save wkt (as it does the geofield) maybe is useful to subscribe to this event and populate another field as text field or taxonomy term field.

## Behavior when using the widget in a extended geofield field(aka GeofieldPolygonItem)
A dedicated field type is provided as it will be useful to store the feature choosen among all the feature collection that is set in the configuration entity. This field type will store all the values of geofield and also these_:
- geojson: GeoJSON Value of the selected feature.
- feature__collection: Feature collection machine name. e.g. cities.
- feature: the name (taken from the value of the key that points the keyholder value) e.g. barcelona.

At field settings you can choose to sync the values defined with another geofield in the same entity, it will sync just before saving the entity. The geofield to choose is in field settings.
At widget settings you can choose which collections are enabled and its order.
Having geojson allows us to create a rich widgets to pick geographical scopes, but despite this seems so easy to work, we loose all the benefits of geofield (such as related modules) because when the field is extended it cannot be used all the formatters that are working with geofield. So to have the best of both worlds you can 

# Developer notes:
What is provided whith this module:

PolygonFeatureCollection Entity: that stores a FeatureCollection geojson and a keyholder that will be used to name the index of each feature. e.g. city_name.
Along with that entity there is:
- ListBuilder: to display the list of these entities.
- WidgetSettingsListBuilder: to display the list of entities in fiedlwidget settings to choose which ones are enabled and to sort they.
- FeatureCollectionStore: a service that is useful to retrieve data from the
  FeatureCollection entities, such as getting a feature with specified key from collection, or getting the value of the key specified in keyholder value from a feature.
- GeofieldPolygonItem : Field type extending geofield to store geojson, feature collection name, and feature index
- PolygonSelectFieldWidget: The widget that allows to choose first the feature collection and after that the feature among all the defined features in that collection. It can work with geofield and with dedicated field type.
- PolygonSelectFieldFormatter: Basic formatter basic with the name of the feature and its collection.
- PolygonSelectFieldSelectedEvent:  The event that is dispatched every time
  a feature is associated with a content(e.g. a node is saved and it has new geofield data).
